//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 09/11/2015.
//
//

#ifndef SINOSCILLATOR_H_INCLUDED
#define SINOSCILLATOR_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include <math.h>

class SinOscillator
{
public:
    
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    /** Return next sine wave sample */
    float getSample();
    
    /** Update the oscillator frequency */
    void setFrequency(float frequency);
    
    /** Update the oscillator amplitude */
    float setAmplitude(float amplitude);
    
    /** Sets note number */
    void setNote (int noteNumber);
    
    /** Sets sample rate */
    void setSampleRate(float sampleRate);
    
    float renderWave (const float currentPhase);
    
private:
    float noteAmplitude;
    float phaseIncrement;
    float phase;
    float sampleRate;
    float twoPi = 2 * M_PI;
    float frequency;
};


#endif /* SINOSCILLATOR_H_INCLUDED */
