//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 09/11/2015.
//
//

#include "SinOscillator.h"
#include <math.h>



SinOscillator::SinOscillator()
{
    phase = 0.f;
    sampleRate = 44100.;
    setFrequency (440.f);
    setAmplitude (0.f);
   
}

SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::getSample()
{
    float out = renderWave (phase ) * noteAmplitude ;
    phase += phaseIncrement;
    if(phase > twoPi)
        phase-= twoPi;
    return out;
}

float SinOscillator::setAmplitude(float amp)
{
    return noteAmplitude = amp;
}

void SinOscillator::setFrequency(float freq)
{
    frequency = freq;
    phaseIncrement = (2 * M_PI * frequency)/sampleRate;
}

void SinOscillator::setNote (int noteNumber)
{
    setFrequency (440.f * pow (2, (noteNumber - 69) / 12.0));
}

void SinOscillator::setSampleRate (float sr)
{
    sampleRate = sr;
    setFrequency(frequency);
}

float SinOscillator::renderWave (const float currentPhase)
{
   return sin (currentPhase);
    
}
