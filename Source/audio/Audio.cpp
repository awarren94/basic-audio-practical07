/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"
#include "SinOscillator.h"
#include <math.h>

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    StringArray sa (MidiInput::getDevices());
    for (String s : sa)
        DBG (s);
    
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
   
    noteVelocity = message.getVelocity();
    oscillator.setNote(message.getNoteNumber());
  
    amp = oscillator.setAmplitude(noteVelocity.get());
    
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];

    
    
    while(numSamples--)
    {

        sample = oscillator.getSample();
        
        *outL = sample;
        *outR = sample;

        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();
    oscillator.setSampleRate(sampleRate);
}

void Audio::audioDeviceStopped()
{

}
void Audio::beep()
{
    oscillator.setAmplitude(1.0);
    oscillator.setFrequency(440.0);
    uint32 currentTime = Time::getMillisecondCounter();
    Time::waitForMillisecondCounter(currentTime +100);
    oscillator.setAmplitude(0.0);
}
