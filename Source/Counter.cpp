//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/11/2015.
//
//

#include "Counter.h"
#include "JuceHeader.h"
#include "MainComponent.h"


Counter::Counter():Thread ("CounterThread")
{
    listener = nullptr;
    counterValue = 0;
    interval = 1;
}

Counter::~Counter()
{
    stopMyThread (0);
}

void Counter::run()
{
    while (!threadShouldExit())
    {
        uint32 currentTime = Time::getMillisecondCounter();
        counterValue = currentTime - startTime;

            if (listener != nullptr)
                listener->counterChanged (counterValue);

        Time::waitForMillisecondCounter (currentTime + interval);
    }
    
}

void Counter::startMyThread()
{
    startThread();
    startTime = Time::getMillisecondCounter();
    counterValue = 0;
}

void Counter::stopMyThread(float ms)
{
   stopThread(500);
    
}

void Counter::addListener (Listener* newListener)
{
    listener = newListener;
}

/** set interval by bpmSlider value */
void Counter::setInterval (int bpm)
{
     interval = 60000 / bpm;
    
}