/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include <stdio.h>


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    setSize (500, 400);
   
    addAndMakeVisible(startButton);
    startButton.setButtonText("START/STOP");
    startButton.addListener(this);
    counter.addListener(this);
   
    addAndMakeVisible(bpmSlider);
    bpmSlider.addListener(this);
    bpmSlider.setRange(10, 250);
    bpmSLider.setSliderStyle(Slider::SliderStyle )
    
    bpm = 10;
    
    /** list midi devices */
    StringArray sa (MidiInput::getDevices());
    for (String s : sa)
        DBG (s);
}

MainComponent::~MainComponent()
{
    //stopThread(500);
}

void MainComponent::resized()
{
    startButton.setBounds(10, 20, getWidth()-40, getHeight()/4);
    bpmSlider.setBounds(10, 100, getWidth()-20, getHeight()/4);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}


void MainComponent::buttonClicked(Button* button)
{
    if(button == &startButton)
    {
        state = startButton.getToggleState();
        DBG(startButton.getToggleState());

        if(state == true)
        {
            counter.startMyThread();
        }
    
        else if(state == false)
        {
            counter.stopMyThread(500);
        }
    }
}


void MainComponent::counterChanged (const unsigned int counterValue)
{
    /** Trigger beep at counter interval */
    audio.beep();
}

/** Slider for BPM control */
void MainComponent::sliderValueChanged(Slider* slider)
{
    if(slider == &bpmSlider)
    {
        counter.setInterval(bpmSlider.getValue());
  
        DBG("value" << bpm );
    }
}

